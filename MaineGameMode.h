// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Alco/Player/PlayerCamerPawn.h"
//#include "Alco/Player/MainPlayerController.h"
//#include "Alco/Core/MasterGameMode.h"
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MaineGameMode.generated.h"

/**
 * 
 */

class ARegionsDataActor;

UCLASS()
class ALCO_API AMaineGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:

	UFUNCTION(BlueprintCallable)
		void PlayerIn(AController* NewPlayer);

	//��������� ������
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameModSettings")
	TSubclassOf <APlayerCamerPawn> CameraPawn;

	int8 PlayerIndexMarker = 0;

	int8 LastPlayerIndex = 0;

	int TotalTimeEnd = 0;
	int VinTime = 910;
	int BlockTime = 10;
	int VinMoney = 499999;

	bool GameIsEnde = false;

	//������ ��������
	TArray <ARegionsDataActor* > RegionList;

	FTimerHandle MineTimer;
	FTimerHandle PawnTimer;

	virtual void ChekcVinPlayer();
	void GameMapInitializstion();

	virtual void TimerFunc();

	void PawnSpecialTimer();

	void BotSpawn();

public:

	bool KefirInGameSpawn = false;

	UFUNCTION(BlueprintCallable)
		void SpawnCamera(uint8 playerid, bool isBot);

	//���������� �� ������������ �������
	UPROPERTY(BlueprintReadOnly)
		TArray<AController* > ConnectedControllers;
	UPROPERTY(BlueprintReadOnly)
		TArray <APlayerCamerPawn*> PalayersPawn{nullptr,nullptr,nullptr};
	UPROPERTY(BlueprintReadOnly)
		TArray <APlayerCamerPawn*> WinPosList;

	int GetVinMoney();

	virtual void FastVin();

	void KefirDrop();

	virtual void BeginPlay() override;

};
