// Fill out your copyright notice in the Description page of Project Settings.

#include "MainPlayerController.h"
#include "Alco/Core/MaineGameMode.h"
#include "Alco/Core/MaineGameInstance.h"
#include "Engine/GameViewportClient.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"


AMainPlayerController::AMainPlayerController()
{
	bShowMouseCursor = true;
}

void AMainPlayerController::BeginPlay()
{
	SetReplicates(true); // ��������� ���������� ����� UPROPERTY(Replicated) �� ��������

	CameraPtr = Cast<APlayerCamerPawn>(GetPawn());
}

void AMainPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis(TEXT("MoveForward"), this, &AMainPlayerController::InputAxisX);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &AMainPlayerController::InputAxisY);


	InputComponent->BindAction(TEXT("MouseRollUp"), EInputEvent::IE_Pressed, this, &AMainPlayerController::MouseRollUp);
	InputComponent->BindAction(TEXT("MouseRollDown"), EInputEvent::IE_Pressed, this, &AMainPlayerController::MouseRollDown);
	InputComponent->BindAction(TEXT("LMC"), EInputEvent::IE_Pressed, this, &AMainPlayerController::LMC);
	InputComponent->BindAction(TEXT("RMC"), EInputEvent::IE_Pressed, this, &AMainPlayerController::RMC);
	InputComponent->BindAction(TEXT("MenueReq"), EInputEvent::IE_Pressed, this, &AMainPlayerController::MenueReqest);
}

void AMainPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//ROC_MouseMovement();

	if (CameraPtr) {

		if (CameraPtr->PlayerId > 0)
		{
			if (!UKismetSystemLibrary::IsServer(GetWorld()))
			{
				MouseMovement(DeltaTime);
			}
		}
		else if(CameraPtr->PlayerId == 0)
		{
			MouseMovement(DeltaTime);
		}
	}

}

// ��������� ���������� ����� UPROPERTY(Replicated) �� ��������
void AMainPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMainPlayerController, MyName);
	DOREPLIFETIME(AMainPlayerController, SelectedHeroe);
	DOREPLIFETIME(AMainPlayerController, PlayerIcon);
	DOREPLIFETIME(AMainPlayerController, SpawnActorClass);
	DOREPLIFETIME(AMainPlayerController, GameInstanceRef);
	DOREPLIFETIME(AMainPlayerController, CameraPtr);
	//DOREPLIFETIME(AMainPlayerController, bLockInput);
}

FName AMainPlayerController::GetMyName_Implementation()
{
	return MyName;
}

void AMainPlayerController::SetMyName_Implementation(FName Name)
{
	MyName = Name;
}

uint8 AMainPlayerController::GetSelectedHeroe_Implementation()
{
	return SelectedHeroe;
}

void AMainPlayerController::SetSelectedHeroe_Implementation(uint8 value)
{
	SelectedHeroe = value;
}

UTexture2D * AMainPlayerController::GetPlayerIcon_Implementation()
{
	return PlayerIcon;
}

void AMainPlayerController::SetPlayerIcon_Implementation(UTexture2D * value)
{
	PlayerIcon = value;
}

TSubclassOf<APlayerCamerPawn> AMainPlayerController::GetSpawnActorClass_Implementation()
{
	return SpawnActorClass;
}

void AMainPlayerController::SetSpawnActorClass_Implementation(TSubclassOf<APlayerCamerPawn> value)
{
	SpawnActorClass = value;
}

bool AMainPlayerController::GetSpawnedVal_Implementation()
{
	return Spawned;
}

void AMainPlayerController::SetSpawnedVal_Implementation(bool val)
{
	Spawned = val;
}

void AMainPlayerController::I_ROC_GetGameInstance(uint8 playerid)
{
	ROC_GetGameInstance(playerid);
}

void AMainPlayerController::I_ROS_GetGameInstance(FName PlayerNameValue, uint8 SelectedHeroeValue, UTexture2D * PlayerIconValue, TSubclassOf<APlayerCamerPawn> SpawnActorClassValue, uint8 playerid)
{
	ROS_GetGameInstance(PlayerNameValue, SelectedHeroeValue, PlayerIconValue, SpawnActorClassValue, playerid);
}

void AMainPlayerController::I_LevelStart()
{
	LevelStart();
}

AController * AMainPlayerController::GetControllerRef()
{
	return this;
}

void AMainPlayerController::LevelStart()
{
	CameraPtr = Cast<APlayerCamerPawn>(GetPawn());

	//bLockInput = false;
}

void AMainPlayerController::InputAxisX(float Value)
{
	//if (!bLockInput){
		if (CameraPtr) {
			CameraPtr->CameraMoveForvard(Value);
		}
	//}
}

void AMainPlayerController::InputAxisY(float Value)
{
	//if (!bLockInput){
		if (CameraPtr) {
			CameraPtr->CameraMoveRight(Value);
		}
	//}
}

void AMainPlayerController::MouseRollUp()
{
	//if (!bLockInput) {
		if (CameraPtr) {
			CameraPtr->MouseRollUp();
		}
	//}
}

void AMainPlayerController::MouseRollDown()
{
	//if (!bLockInput) {
		if (CameraPtr) {
			CameraPtr->MouseRollDown();
		}
	//}
}

void AMainPlayerController::LMC()
{
	if (CameraPtr) {
		CameraPtr->LMC_Press();
	}
}

void AMainPlayerController::RMC()
{
	if (CameraPtr) {
		CameraPtr->RMC_Press();
	}
}

void AMainPlayerController::MenueReqest()
{
	if (CameraPtr) {
		CameraPtr->MenueReqest();
	}
}

void AMainPlayerController::MouseMovement(float DT)
{
	//if (!bLockInput) {
		float OffsetX = 0.f;
		float OffsetY = 0.f;
		FVector2D ScreenSize;
		UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
		if (ViewportClient) {
			ViewportClient->GetViewportSize(ScreenSize);
			GetMousePosition(MousePosX, MousePosY);
			if (((ScreenSize.X - MousePosX) / ScreenSize.X) >= 0.99f) {
				OffsetX = -1.f;
			}
			else if (((ScreenSize.X - MousePosX) / ScreenSize.X) <= 0.01f) {
				OffsetX = 1.f;
			}
			if (((ScreenSize.Y - MousePosY) / ScreenSize.Y) >= 0.99f) {
				OffsetY = 1.f;
			}
			else if (((ScreenSize.Y - MousePosY) / ScreenSize.Y) <= 0.01f) {
				OffsetY = -1.f;
			}
			if (CameraPtr) {
				CameraPtr->MouseMove(OffsetX, OffsetY, DT);
			}
		}
	//}
}



void AMainPlayerController::ROC_GetGameInstance_Implementation(uint8 playerid)
{
	//UMaineGameInstance* ClientGameInstanceRef = Cast<UMaineGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	UMaineGameInstance* ClientGameInstanceRef = Cast<UMaineGameInstance>(GetWorld()->GetGameInstance());
	if (ClientGameInstanceRef) {
		ROS_GetGameInstance(ClientGameInstanceRef->MyName, ClientGameInstanceRef->SelectedHeroe, ClientGameInstanceRef->PlayerIcon, ClientGameInstanceRef->SpawnActorClass, playerid);
	}
	
}

void AMainPlayerController::ROS_GetGameInstance_Implementation(FName PlayerNameValue, uint8 SelectedHeroeValue, UTexture2D * PlayerIconValue, TSubclassOf<APlayerCamerPawn> SpawnActorClassValue, uint8 playerid)
{
	MyName = PlayerNameValue;
	SelectedHeroe = SelectedHeroeValue;
	PlayerIcon = PlayerIconValue;
	SpawnActorClass = SpawnActorClassValue;

	AMaineGameMode *GM = Cast<AMaineGameMode>(UGameplayStatics::GetGameMode(GetWorld()));

	if (GM) {
		GM->SpawnCamera(playerid, false);
	}
}