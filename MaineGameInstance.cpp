// Fill out your copyright notice in the Description page of Project Settings.


#include "MaineGameInstance.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
//#include "OnlineSubsystemSteam.h"

UMaineGameInstance::UMaineGameInstance() 
{
	BotList.Init(FBotInfo(), 2);
}


//############################################# ������������� ������ #########################################################
void UMaineGameInstance::SetData(int SelectedHeroeValue, UTexture2D * PlayerIconValue, TSubclassOf<APlayerCamerPawn> SpawnActorClassValue)
{
	SelectedHeroe = SelectedHeroeValue;
	PlayerIcon = PlayerIconValue;
	SpawnActorClass = SpawnActorClassValue;
}

void UMaineGameInstance::SetBotData(FName BotName, int SelectedHeroeValue, UTexture2D * PlayerIconValue, TSubclassOf<APlayerCamerPawn> SpawnActorClassValue, TSubclassOf<AMasterAIController> SpawnControllerClass)
{
	for (FBotInfo& element : BotList)
	{
		if (!element.SlotSelected)
		{
			element.SlotSelected = true;
			element.MyName = BotName;
			element.SelectedHeroe = SelectedHeroeValue;
			element.PlayerIcon = PlayerIconValue;
			element.SpawnActorClass = SpawnActorClassValue;
			element.SpawnControllerClass = SpawnControllerClass;
			break;
		}
	}
}


//############################################# ������������� � ����� #########################################################
void UMaineGameInstance::Init()
{
	if (IOnlineSubsystem* SubSyatem = IOnlineSubsystem::Get())
	{
		SessionInterface = SubSyatem->GetSessionInterface();
		if (SessionInterface.IsValid())
		{
			// ����������
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UMaineGameInstance::OnCreateSessionComplite);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UMaineGameInstance::OnSerchSessionComplite);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UMaineGameInstance::OnJoinSessionComplite);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UMaineGameInstance::OnDestroySessionComplite);

			// ����� �� �����
			SessionInviteAcceptedDelegate = FOnSessionUserInviteAcceptedDelegate::CreateUObject(this, &UMaineGameInstance::OnInviteAsscept);
			SessionInterface->AddOnSessionUserInviteAcceptedDelegate_Handle(SessionInviteAcceptedDelegate);

		}
	}
}
void UMaineGameInstance::OnCreateSessionComplite(FName ServerName, bool Sucseeded)
{
	if (Sucseeded) 
	{

		GetWorld()->ServerTravel("/Game/MAPS/MainLobby_Level?listen");
	}
	else
	{
		if (AutoMMOn)
		{
			AutoMMCreationFail();
		}
		else
		{
			CreationFail();
		}
	}
}
void UMaineGameInstance::OnSerchSessionComplite(bool Sucseeded)
{
	if (Sucseeded)
	{
		SearchResult.Empty();
		SessionInfoList.Empty();
		SearchResult = SessionSerch->SearchResults;

		if (AutoMMOn)
		{
			AutoMMindex = -1;
			AutoMMTryConnect();

		}
		else
		{

			if (SByNam)
			{

				int32 ServerArrayIndexTemp = -1;
				SessionInfoList.Num();

				for (FOnlineSessionSearchResult element : SessionSerch->SearchResults)
				{
					ServerArrayIndexTemp++;
					if (!element.IsValid())
					{
						continue;
					}

					FServerInfo Info;
					bool isPrivate;
					bool isStarted;
					FString ServerName;
					int PCount;

					element.Session.SessionSettings.Get(FName("SERVER_NAME_KEY"), ServerName);
					element.Session.SessionSettings.Get(FName("SERVER_PRIVATE_KEY"), isPrivate);
					element.Session.SessionSettings.Get(FName("SERVER_SESSIONSTART_KEY"), isStarted);
					element.Session.SessionSettings.Get(FName("SERVER_PLAYER_KEY"), PCount);

					if (element.Session.SessionSettings.Get(FName("SERVER_NAME_KEY"), ServerName))
					{
						Info.ServerName = ServerName;
					}
					else
					{
						Info.ServerName = "Could not find name.";
					}

					Info.isPrivate = isPrivate;
					Info.isStarted = isStarted;
					Info.MaxPlayer = element.Session.SessionSettings.NumPublicConnections; 
					Info.CurrentPlayer = PCount;
					Info.ServerArrayIndex = ServerArrayIndexTemp;

					if (Info.ServerName == FinServerName)
					{
						SessionInfoList.Add(Info);
					}

				}

			}
			else
			{
				int32 ServerArrayIndexTemp = -1;
				SessionInfoList.Num();

				for (FOnlineSessionSearchResult element : SessionSerch->SearchResults)
				{
					ServerArrayIndexTemp++;
					if (!element.IsValid())
					{
						continue;
					}

					FServerInfo Info;
					bool isPrivate;
					bool isStarted;
					FString ServerName;
					int PCount;

					element.Session.SessionSettings.Get(FName("SERVER_NAME_KEY"), ServerName);
					element.Session.SessionSettings.Get(FName("SERVER_PRIVATE_KEY"), isPrivate);
					element.Session.SessionSettings.Get(FName("SERVER_SESSIONSTART_KEY"), isStarted);
					element.Session.SessionSettings.Get(FName("SERVER_PLAYER_KEY"), PCount);

					if (isPrivate || isStarted)
					{
						continue;
					}


					if (element.Session.SessionSettings.Get(FName("SERVER_NAME_KEY"), ServerName))
					{
						Info.ServerName = ServerName;
					}
					else
					{
						Info.ServerName = "Could not find name.";
					}

					Info.isPrivate = isPrivate;
					Info.isStarted = isStarted;
					Info.MaxPlayer = element.Session.SessionSettings.NumPublicConnections;
					Info.CurrentPlayer = PCount;
					Info.ServerArrayIndex = ServerArrayIndexTemp;


					SessionInfoList.Add(Info);

				}
			}

			ServerListDel.Broadcast(SessionInfoList);
		}

	}
	else
	{
		if (AutoMMOn)
		{
			AutoMMFail();
		}
	}
}
void UMaineGameInstance::OnJoinSessionComplite(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{


	if (Result == EOnJoinSessionCompleteResult::Type::Success) {

		if (SelectedSession.IsValid())
		{
			SelectedSession.Session.NumOpenPublicConnections++; //����������� ����� ������������ �������
		}


		if (APlayerController* PCcontrol = UGameplayStatics::GetPlayerController(GetWorld(), 0))
		{
			FString JoinAddres;
			SessionInterface->GetResolvedConnectString(SessionName, JoinAddres);
			if (JoinAddres != "")
			{
				PCcontrol->ClientTravel(JoinAddres, ETravelType::TRAVEL_Absolute);
			}
		}
	}
	else
	{
		if (AutoMMOn)
		{
			AutoMMTryConnect();
		}
		else
		{
			ConFail();
		}
	}

}
void UMaineGameInstance::OnInviteAsscept(const bool bWasSuccessful, int32 LocalPlayer, TSharedPtr<const FUniqueNetId> PersonInviting, const FOnlineSessionSearchResult & SessionToJoin)
{

	if (bWasSuccessful)
	{
		if (SessionToJoin.IsValid())
		{

			SelectedSession = SessionToJoin;

			//isHostCreation = false;

			auto ExistingSession = SessionInterface->GetNamedSession(SesseonName);

			if (ExistingSession != nullptr)
			{
				DestrMode = EDestroyMode::DestroyAndConnection;
				SessionInterface->DestroySession(SesseonName);
			}
			else
			{
				SessionConnect();
			}

		}

	}

}
void UMaineGameInstance::OnDestroySessionComplite(FName ServerName, bool Sucseeded)
{

	if (Sucseeded)
	{
		switch (DestrMode)
		{
		case EDestroyMode::NormalDestroy:
			break;
		case EDestroyMode::DestroyAndConnection:
		{
			SessionConnect();
			break;
		}
		case EDestroyMode::DestroyAndCreateSession:
		{
			CreateServer();
			break;
		}
		case EDestroyMode::DestroyAndAutoMMCreateSession:
		{
			AutoMMCreateSession();
			break;
		}
		}
	}


	
	/*
	if (Sucseeded)
	{
		if (!DestrOnlu) {

			if (isHostCreation)
			{
				SessionCreate();
			}
			else
			{
				SessionConnect();
			}
		}
	}
	*/
}



// ############################################# ������������� � ����� #########################################################
void UMaineGameInstance::SetSessionPrivate()
{

	IOnlineSessionPtr Ses = Online::GetSessionInterface(GetWorld());
	if (Ses.IsValid())
	{
		FOnlineSessionSettings* Settings = Ses->GetSessionSettings(SesseonName);

		if (Settings)
		{
			FOnlineSessionSetting * fSetting = NULL;
			FVariantData NewData;
			NewData.SetValue(true);

			fSetting = Settings->Settings.Find(FName("SERVER_PRIVATE_KEY"));

			if (fSetting)
			{
				fSetting->Data = NewData;
			}

			Ses->UpdateSession(SesseonName, *Settings, true);

		}
	}

}
void UMaineGameInstance::SetSessionPublic()
{
	IOnlineSessionPtr Ses = Online::GetSessionInterface(GetWorld());
	if (Ses.IsValid())
	{
		FOnlineSessionSettings* Settings = Ses->GetSessionSettings(SesseonName);

		if (Settings)
		{
			FOnlineSessionSetting * fSetting = NULL;
			FVariantData NewData;
			NewData.SetValue(false);

			fSetting = Settings->Settings.Find(FName("SERVER_PRIVATE_KEY"));

			if (fSetting)
			{
				fSetting->Data = NewData;
			}

			Ses->UpdateSession(SesseonName, *Settings, true);

		}
	}
}
void UMaineGameInstance::SetSessionStarted()
{
	IOnlineSessionPtr Ses = Online::GetSessionInterface(GetWorld());
	if (Ses.IsValid())
	{
		FOnlineSessionSettings* Settings = Ses->GetSessionSettings(SesseonName);

		if (Settings)
		{
			FOnlineSessionSetting * fSetting = NULL;
			FVariantData NewData;
			NewData.SetValue(true);

			fSetting = Settings->Settings.Find(FName("SERVER_SESSIONSTART_KEY"));

			if (fSetting)
			{
				fSetting->Data = NewData;
			}


			Ses->UpdateSession(SesseonName, *Settings, true);

		}
	}
}

//############################################# ������������� � ����� #########################################################

void UMaineGameInstance::CreateServer()
{
	AutoMMOn = false;

	if (SessionInterface.IsValid())
	{
		//isHostCreation = true;

		auto ExistingSession = SessionInterface->GetNamedSession(SesseonName);

		if (ExistingSession != nullptr) 
		{
			// ���� ��� �������� ������� �� ����� �� �������� ���������� ������ �� � ���� ���������
			DestrMode = EDestroyMode::DestroyAndCreateSession;
			SessionInterface->DestroySession(SesseonName);
		}
		else
		{
			// ���� ������ ��������� �� ������ �����
			SessionCreate();
		}
	}
}
void UMaineGameInstance::SessionCreate()
{
	FOnlineSessionSettings SessionSettings;
	SessionSettings.bIsLANMatch = false;
	SessionSettings.bAllowJoinInProgress = true;
	//SessionSettings.bIsDedicated = false;
	SessionSettings.NumPublicConnections = 3;
	SessionSettings.bShouldAdvertise = true;
	SessionSettings.bUsesPresence = true;

	SessionSettings.Set(FName("SERVER_NAME_KEY"), MyName.ToString(), EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);
	SessionSettings.Set(FName("SERVER_PRIVATE_KEY"), false, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);
	SessionSettings.Set(FName("SERVER_SESSIONSTART_KEY"), false, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);
	SessionSettings.Set(FName("SERVER_PLAYER_KEY"), 0, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

	SessionInterface->CreateSession(0, SesseonName, SessionSettings);
}


//############################################# ����� ������ #########################################################
void UMaineGameInstance::SerchServer()
{
	AutoMMOn = false;
	SByNam = false;
	SerchAlog();
}
void UMaineGameInstance::SerchServerByName(FString FindServerName)
{
	AutoMMOn = false;
	SByNam = true;
	FinServerName = FindServerName;
	SerchAlog();
}
void UMaineGameInstance::SerchAlog()
{

	SessionSerch = MakeShareable(new FOnlineSessionSearch());

	//SessionSerch->bIsLanQuery = true;
	SessionSerch->MaxSearchResults = 50;
	SessionSerch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);

	SessionInterface->FindSessions(0, SessionSerch.ToSharedRef());

}


//############################################# ����������� � ������ #########################################################

void UMaineGameInstance::JoinServer(const int32& SessionIndex) //����������� � �������
{
	SelectedSession = SearchResult[SessionIndex];

	if (SelectedSession.IsValid())
	{

		//isHostCreation = false;

		auto ExistingSession = SessionInterface->GetNamedSession(SesseonName);

		if (ExistingSession != nullptr)
		{
			DestrMode = EDestroyMode::DestroyAndConnection;
			SessionInterface->DestroySession(SesseonName);
		}
		else
		{
			SessionConnect();
		}
	}

}
void UMaineGameInstance::SessionConnect()
{
	if (SelectedSession.IsValid())
	{
		SessionInterface->JoinSession(0, SesseonName, SelectedSession);
	}
}

//############################################# ���������� ������ #########################################################
void UMaineGameInstance::DestroySesOnly()
{
	DestrMode = EDestroyMode::NormalDestroy;
	SessionInterface->DestroySession(SesseonName);
}

//############################################# ��������� #########################################################
void UMaineGameInstance::AutoMM()
{
	AutoMMOn = true;

	SerchAlog();
}
void UMaineGameInstance::AutoMMTryConnect()
{
	AutoMMindex++;

	if ((AutoMMindex + 1) <= SearchResult.Num()) 
	{

		SelectedSession = SearchResult[AutoMMindex];

		bool isPrivate;
		bool isStarted;
		int PCount;
		int MP = SelectedSession.Session.SessionSettings.NumPublicConnections;

		SelectedSession.Session.SessionSettings.Get(FName("SERVER_PRIVATE_KEY"), isPrivate);
		SelectedSession.Session.SessionSettings.Get(FName("SERVER_SESSIONSTART_KEY"), isStarted);
		SelectedSession.Session.SessionSettings.Get(FName("SERVER_PLAYER_KEY"), PCount);

		if (!isPrivate && !isStarted && PCount<=MP)
		{
			//isHostCreation = false;

			auto ExistingSession = SessionInterface->GetNamedSession(SesseonName);

			if (ExistingSession != nullptr)
			{
				DestrMode = EDestroyMode::DestroyAndConnection;
				SessionInterface->DestroySession(SesseonName);
			}
			else
			{
				SessionConnect();
			}
		}

	}
	else
	{
		AutoMMCreateSession();
	}

}
void UMaineGameInstance::AutoMMCreateSession()
{

	if (SessionInterface.IsValid())
	{
		//isHostCreation = true;

		auto ExistingSession = SessionInterface->GetNamedSession(SesseonName);

		if (ExistingSession != nullptr)
		{
			DestrMode = EDestroyMode::DestroyAndAutoMMCreateSession;
			SessionInterface->DestroySession(SesseonName);
		}
		else
		{
			SessionCreate();
		}
	}
}




void UMaineGameInstance::ConnectionUp()
{
	IOnlineSessionPtr Ses = Online::GetSessionInterface(GetWorld());
	if (Ses.IsValid())
	{
		FOnlineSessionSettings* Settings = Ses->GetSessionSettings(SesseonName);

		if (Settings)
		{
			FOnlineSessionSetting * fSetting = NULL;
			FVariantData NewData;
			int PCount;
			
			Settings->Get(FName("SERVER_PLAYER_KEY"), PCount);

			PCount++;

			NewData.SetValue(PCount);

			fSetting = Settings->Settings.Find(FName("SERVER_PLAYER_KEY"));

			if (fSetting)
			{
				
				fSetting->Data = NewData;
			}


			Ses->UpdateSession(SesseonName, *Settings, true);

		}
	}
}

void UMaineGameInstance::ConnectionDown()
{
	IOnlineSessionPtr Ses = Online::GetSessionInterface(GetWorld());
	if (Ses.IsValid())
	{
		FOnlineSessionSettings* Settings = Ses->GetSessionSettings(SesseonName);

		if (Settings)
		{
			FOnlineSessionSetting * fSetting = NULL;
			FVariantData NewData;
			int PCount;

			Settings->Get(FName("SERVER_PLAYER_KEY"), PCount);

			PCount--;

			NewData.SetValue(PCount);

			fSetting = Settings->Settings.Find(FName("SERVER_PLAYER_KEY"));

			if (fSetting)
			{

				fSetting->Data = NewData;
			}


			Ses->UpdateSession(SesseonName, *Settings, true);

		}
	}
}




void UMaineGameInstance::CreationFail_Implementation()
{
}

void UMaineGameInstance::ConFail_Implementation()
{
}

void UMaineGameInstance::AutoMMFail_Implementation()
{
}

void UMaineGameInstance::AutoMMCreationFail_Implementation()
{
}


