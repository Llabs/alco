// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/Texture2D.h"
#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "OnlineSubsystemUtils.h"
#include "Math/UnrealMathUtility.h"
#include "MaineGameInstance.generated.h"

class APlayerCamerPawn;
class AMasterAIController;


UENUM(BlueprintType)
enum class EDestroyMode : uint8
{
	NormalDestroy,
	DestroyAndCreateSession,
	DestroyAndConnection,
	DestroyAndAutoMMCreateSession
};


USTRUCT(BlueprintType)
struct FServerInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly)
		FString ServerName;
	UPROPERTY(BlueprintReadOnly)
		int32 CurrentPlayer;
	UPROPERTY(BlueprintReadOnly)
		int32 MaxPlayer;
	UPROPERTY(BlueprintReadOnly)
		int32 ServerArrayIndex;
	UPROPERTY(BlueprintReadOnly)
		bool isPrivate;
	UPROPERTY(BlueprintReadOnly)
		bool isStarted;

};


USTRUCT(BlueprintType)
struct FBotInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite)
		bool SlotSelected = false;
	UPROPERTY(BlueprintReadWrite)
		FName MyName;
	UPROPERTY(BlueprintReadWrite)
		int SelectedHeroe = -1;
	UPROPERTY(BlueprintReadWrite)
		UTexture2D* PlayerIcon = nullptr;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TSubclassOf<APlayerCamerPawn> SpawnActorClass;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TSubclassOf<AMasterAIController> SpawnControllerClass;

};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FServerDel, const TArray<FServerInfo>&, ServerListDel);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSerchFin);

/**
 * 
 */
UCLASS()
class ALCO_API UMaineGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

//############################################# ������������� ������ #########################################################
	UMaineGameInstance();
	UPROPERTY(BlueprintReadWrite)
		FName MyName;
	UPROPERTY(BlueprintReadWrite)
		int PlayerId = -1;
	UPROPERTY(BlueprintReadWrite)
		UMaterial* PlayerColor = nullptr;
	UPROPERTY(BlueprintReadWrite)
		int SelectedHeroe = -1;
	UPROPERTY(BlueprintReadWrite)
		UTexture2D* PlayerIcon = nullptr;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TSubclassOf<APlayerCamerPawn> SpawnActorClass;

	TArray<FBotInfo> BotList;

	UFUNCTION(BlueprintCallable)
		void SetData(int SelectedHeroeValue, UTexture2D* PlayerIconValue, TSubclassOf<APlayerCamerPawn> SpawnActorClassValue);

	UFUNCTION(BlueprintCallable)
		void SetBotData(FName BotName, int SelectedHeroeValue, UTexture2D* PlayerIconValue, TSubclassOf<APlayerCamerPawn> SpawnActorClassValue, TSubclassOf<AMasterAIController> SpawnControllerClass);

//###################################################################################################################
protected:

	const FName SesseonName = FName(*FString::FromInt(FMath::RandRange(100000, 10000000)));

	UPROPERTY(BlueprintAssignable)
		FServerDel ServerListDel;
	//UPROPERTY(BlueprintAssignable)
	//	FSerchFin SerchFinList;

	//bool isHostCreation = false;


	UPROPERTY(BlueprintReadOnly)
	bool AutoMMOn = false;
	int AutoMMindex = -1;

	bool SByNam = false;
	FString FinServerName;

	EDestroyMode DestrMode = EDestroyMode::NormalDestroy;


	FOnlineSessionSearchResult SelectedSession;

	IOnlineSessionPtr SessionInterface;
	TSharedPtr<FOnlineSessionSearch> SessionSerch;
	TArray<FOnlineSessionSearchResult> SearchResult;
	TArray<FServerInfo> SessionInfoList;

	FOnSessionUserInviteAcceptedDelegate SessionInviteAcceptedDelegate;

//############################################# ������������� � ����� #########################################################
	virtual void Init() override;
	virtual void OnCreateSessionComplite(FName ServerName, bool Sucseeded);
	virtual void OnSerchSessionComplite(bool Sucseeded);
	virtual void OnJoinSessionComplite(FName SessionName, EOnJoinSessionCompleteResult::Type Result);
	virtual void OnInviteAsscept(const bool bWasSuccessful, int32 LocalPlayer, TSharedPtr<const FUniqueNetId> PersonInviting, const FOnlineSessionSearchResult& SessionToJoin);
	virtual void OnDestroySessionComplite(FName ServerName, bool Sucseeded);
//###################################################################################################################

// ############################################# ������������� � ����� #########################################################
	UFUNCTION(BlueprintCallable)
		void SetSessionPrivate();
	UFUNCTION(BlueprintCallable)
		void SetSessionPublic();
	UFUNCTION(BlueprintCallable)
		void SetSessionStarted();
//###################################################################################################################

//############################################# ������������� � ����� #########################################################
	UFUNCTION(BlueprintCallable)
		void CreateServer();
		void SessionCreate();

//###################################################################################################################

//############################################# ����� ������ #########################################################
		UFUNCTION(BlueprintCallable)
			void SerchServer();
		UFUNCTION(BlueprintCallable)
			void SerchServerByName(FString FindServerName);
		void SerchAlog();

//###################################################################################################################

//############################################# ����������� � ������ #########################################################
		UFUNCTION(BlueprintCallable)
			void JoinServer(const int32& SessionIndex);
		void SessionConnect();

//###################################################################################################################	

//############################################# ���������� ������ #########################################################
		UFUNCTION(BlueprintCallable)
			void DestroySesOnly();
//###################################################################################################################
	
//############################################# ��������� #########################################################
	UFUNCTION(BlueprintCallable)
		void AutoMM();		
	void AutoMMTryConnect();
	void AutoMMCreateSession();
//###################################################################################################################



	UFUNCTION(BlueprintNativeEvent)
		void CreationFail(); // ���� �������� ������ ����������� ��������
	UFUNCTION(BlueprintNativeEvent)
		void ConFail(); // ���� ����������� ����������� ��������
	UFUNCTION(BlueprintNativeEvent)
		void AutoMMFail();
	UFUNCTION(BlueprintNativeEvent)
		void AutoMMCreationFail();


	UFUNCTION(BlueprintCallable)
		void ConnectionUp();
	UFUNCTION(BlueprintCallable)
		void ConnectionDown();

};
