// Fill out your copyright notice in the Description page of Project Settings.

#include "MaineGameMode.h"
#include "Alco/Core/MaineGameInstance.h"
#include "Alco/Core/SpawnPoint.h"
#include "Alco/Ai/MasterAIController.h"
#include "Alco/Player/MainPlayerController.h"
#include"Alco/Regions/RegionsDataActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Engine/TargetPoint.h"
#include "Kismet/GameplayStatics.h"




int AMaineGameMode::GetVinMoney()
{
	return VinMoney + 1;
}

void AMaineGameMode::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(MineTimer, this, &AMaineGameMode::TimerFunc, 1.0f, true);

	GetWorld()->GetTimerManager().SetTimer(PawnTimer, this, &AMaineGameMode::PawnSpecialTimer, 0.1f, true);

	GameMapInitializstion();

	BotSpawn();
	
}


void AMaineGameMode::PlayerIn(AController* NewPlayer)
{
	II_Controller* PlayerPtr = Cast<II_Controller>(NewPlayer);

	if (PlayerPtr) {  // ��������� ��� ���������� ����� ������� ��� ����, ����� �� ��������� ������������
		ConnectedControllers.Add(NewPlayer);
		PlayerPtr->I_ROC_GetGameInstance(PlayerIndexMarker);
		PlayerIndexMarker++;
	}
}


void AMaineGameMode::ChekcVinPlayer()
{ 
	WinPosList.Empty();
	//WinPosList.Append(PalayersPawn);

	for (APlayerCamerPawn* element : PalayersPawn)
	{
		if (element) 
		{
			WinPosList.Add(element);
		}
	}


	// ����������
	WinPosList.Sort([](const APlayerCamerPawn& A, const APlayerCamerPawn& B) {
		return A.TotalMoney > B.TotalMoney;
		});

	//APlayerCamerPawn*  WinPawn = nullptr;

	// �������� ������ �� �������
	if (WinPosList[0]->TotalMoney > VinMoney) 
	{
		GameIsEnde = true;

		FName EmptyName;

		for (uint8 i = 0; i < WinPosList.Num(); i++) {

			if (WinPosList.Num() >= 3)
			{
				WinPosList[i]->ROC_GameEnd(WinPosList[0]->PlayerId, WinPosList[0]->PlayerIcon, WinPosList[0]->MyName,
											WinPosList[1]->PlayerId, WinPosList[1]->PlayerIcon, WinPosList[1]->MyName,
											WinPosList[2]->PlayerId, WinPosList[2]->PlayerIcon, WinPosList[2]->MyName);
			}
			else if (WinPosList.Num() == 2)
			{
				WinPosList[i]->ROC_GameEnd(WinPosList[0]->PlayerId, WinPosList[0]->PlayerIcon, WinPosList[0]->MyName,
					WinPosList[1]->PlayerId, WinPosList[1]->PlayerIcon, WinPosList[1]->MyName,
					-1, nullptr, EmptyName);
				WinPosList[i]->PawnControlOff = true;

			}
			else if (WinPosList.Num() == 1)
			{
				WinPosList[i]->ROC_GameEnd(WinPosList[0]->PlayerId, WinPosList[0]->PlayerIcon, WinPosList[0]->MyName,
					-1, nullptr, EmptyName,
					-1, nullptr, EmptyName);
				WinPosList[i]->PawnControlOff = true;
			}
		}

	}

	// ������ �� �������
	if (TotalTimeEnd >= VinTime) {

		GameIsEnde = true;

		FName EmptyName;

		for (uint8 i = 0; i < WinPosList.Num(); i++) {

			if (WinPosList.Num() >= 3)
			{
				WinPosList[i]->ROC_GameEnd(WinPosList[0]->PlayerId, WinPosList[0]->PlayerIcon, WinPosList[0]->MyName,
					WinPosList[1]->PlayerId, WinPosList[1]->PlayerIcon, WinPosList[1]->MyName,
					WinPosList[2]->PlayerId, WinPosList[2]->PlayerIcon, WinPosList[2]->MyName);
			}
			else if (WinPosList.Num() == 2)
			{
				WinPosList[i]->ROC_GameEnd(WinPosList[0]->PlayerId, WinPosList[0]->PlayerIcon, WinPosList[0]->MyName,
					WinPosList[1]->PlayerId, WinPosList[1]->PlayerIcon, WinPosList[1]->MyName,
					-1, nullptr, EmptyName);
			}
			else if (WinPosList.Num() == 1)
			{
				WinPosList[i]->ROC_GameEnd(WinPosList[0]->PlayerId, WinPosList[0]->PlayerIcon, WinPosList[0]->MyName,
					-1, nullptr, EmptyName,
					-1, nullptr, EmptyName);
			}
		}

	}
	
	
}

void AMaineGameMode::FastVin()
{
	//����������� ������
	TArray <APlayerCamerPawn*> TempPosList;
	//TempPosList.Append(PalayersPawn);

	for (APlayerCamerPawn* element : PalayersPawn)
	{
		if (element)
		{
			TempPosList.Add(element);
		}
	}


	// ����������
	TempPosList.Sort([](const APlayerCamerPawn& A, const APlayerCamerPawn& B) {
		return A.TotalMoney > B.TotalMoney;
		});

	GameIsEnde = true;


	FName EmptyName;

	for (uint8 i = 0; i < TempPosList.Num(); i++) {

		if (TempPosList.Num() >= 3)
		{
			TempPosList[i]->ROC_GameEnd(TempPosList[0]->PlayerId, TempPosList[0]->PlayerIcon, TempPosList[0]->MyName,
				TempPosList[1]->PlayerId, TempPosList[1]->PlayerIcon, TempPosList[1]->MyName,
				TempPosList[2]->PlayerId, TempPosList[2]->PlayerIcon, TempPosList[2]->MyName);
		}
		else if (TempPosList.Num() == 2)
		{
			TempPosList[i]->ROC_GameEnd(TempPosList[0]->PlayerId, TempPosList[0]->PlayerIcon, TempPosList[0]->MyName,
				TempPosList[1]->PlayerId, TempPosList[1]->PlayerIcon, TempPosList[1]->MyName,
				-1, nullptr, EmptyName);
		}
		else if (TempPosList.Num() == 1)
		{
			TempPosList[i]->ROC_GameEnd(TempPosList[0]->PlayerId, TempPosList[0]->PlayerIcon, TempPosList[0]->MyName,
				-1, nullptr, EmptyName,
				-1, nullptr, EmptyName);
		}
	}

}

void AMaineGameMode::GameMapInitializstion()
{
	// �������� ��� �������
	TSubclassOf<ARegionsDataActor> SomeActorClass;
	SomeActorClass = ARegionsDataActor::StaticClass();
	TArray< AActor * >  OutActors;
	UGameplayStatics::GetAllActorsOfClass(this, SomeActorClass, OutActors);

	for (AActor* SomeActor : OutActors) {
		ARegionsDataActor* temp = Cast<ARegionsDataActor>(SomeActor);
		if (temp) {
			RegionList.Add(temp);
		}
	}
}

void AMaineGameMode::SpawnCamera(uint8 playerid, bool isBot)
{
	// ���� �������� ����� ������ �������
	TSubclassOf<ATargetPoint> classToFind;
	classToFind = ASpawnPoint::StaticClass();
	TArray<AActor*> foundEnemies;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), classToFind, foundEnemies);

	// ���� ������� ������ ������

	bool PointFound = false;

	ASpawnPoint* SpawnPoint = nullptr;

	for (AActor* element : foundEnemies) {
		ASpawnPoint* temp = Cast<ASpawnPoint>(element);

		if (temp) {
			if (temp->PointIndex == playerid) {
				SpawnPoint = temp;
				PointFound = true;
			}
		}
	}

	if (PointFound) {
		FVector SpawnLocation(SpawnPoint->GetActorLocation());
		FRotator SpawnRotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		II_Controller* ControllerInterface = Cast<II_Controller>(ConnectedControllers[playerid]);

		if (ControllerInterface) {

			CameraPawn = II_Controller::Execute_GetSpawnActorClass(ConnectedControllers[playerid]);

			if (CameraPawn) {
				
				//PalayersPawn.Add(Cast<APlayerCamerPawn>(GetWorld()->SpawnActor(CameraPawn, &SpawnLocation, &SpawnRotation, SpawnParams)));

				APlayerCamerPawn* NewCamera = Cast<APlayerCamerPawn>(GetWorld()->SpawnActor(CameraPawn, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (NewCamera) {
					PalayersPawn[playerid] = NewCamera;
					NewCamera->PlayerId = playerid;
					NewCamera->PlayerIcon = II_Controller::Execute_GetPlayerIcon(ConnectedControllers[playerid]);
					NewCamera->MyName = II_Controller::Execute_GetMyName(ConnectedControllers[playerid]);
					NewCamera->KefirAddInGame = KefirInGameSpawn; // ��������� �������� ������

					//���� �������� ���������� ������ � ������
					AController* SelectedController = ControllerInterface->GetControllerRef();
					SelectedController->Possess(NewCamera);

					ControllerInterface->I_LevelStart(); // �������  ������� ����������� ��� �� ��������� � �������� ����������

					PalayersPawn[playerid]->ROC_ClientStartSetting(isBot);
					//PalayersPawn[playerid]->MC_SetColorById();
					//PalayersPawn[LastPlayerIndex]->ROC_PlayerCardInit(); // �������������� ��������
				}
				else
				{
					ControllerInterface->I_ROC_GetGameInstance(PlayerIndexMarker);
				}

			}
			else
			{
				ControllerInterface->I_ROC_GetGameInstance(PlayerIndexMarker);
			}

		}
	}

}
void AMaineGameMode::TimerFunc()
{

	if (!GameIsEnde) {
		TotalTimeEnd++;

		for (ARegionsDataActor* Region : RegionList) {
			Region->SecondEnde();
		}

		for (APlayerCamerPawn* Cam : PalayersPawn) 
		{
			if (Cam) {
				Cam->EffectComponent->ComponentSecondEnd();
				Cam->RestoreLapkiFunk();
				Cam->MoneyChenge(Cam->Prirost);
				Cam->timeInfo = VinTime - TotalTimeEnd;

				if (TotalTimeEnd >= BlockTime && Cam->WatePlayerCheck)
				{
					Cam->WatePlayerCheck = false;
				}
			}
		}

		ChekcVinPlayer();
	}
	
}

void AMaineGameMode::PawnSpecialTimer()
{

	for (APlayerCamerPawn* Cam : PalayersPawn) {

		if (Cam) {
			Cam->CharakterSpecialFunk();
		}
	}
	
}

void AMaineGameMode::BotSpawn()
{
	UMaineGameInstance* GameInstanceRef = Cast<UMaineGameInstance>(GetWorld()->GetGameInstance());
	if (GameInstanceRef)
	{
		TArray<FBotInfo> TEMP = GameInstanceRef->BotList;

		for (FBotInfo& element : GameInstanceRef->BotList)
		{
			if (element.SlotSelected)
			{
				AController* AiController = nullptr;
				{
					FVector SpawnLocation(0.0f, 0.0f, 0.0f);
					FRotator SpawnRotation(0.0f, 0.0f, 0.0f);
					FActorSpawnParameters SpawnParams;
					SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
					AiController = Cast<AController>(GetWorld()->SpawnActor(element.SpawnControllerClass, &SpawnLocation, &SpawnRotation, SpawnParams));

					II_Controller::Execute_SetMyName(AiController, element.MyName);
					II_Controller::Execute_SetSpawnActorClass(AiController, element.SpawnActorClass);
					II_Controller::Execute_SetPlayerIcon(AiController, element.PlayerIcon);
					II_Controller::Execute_SetSelectedHeroe(AiController, element.SelectedHeroe);

				}

				II_Controller* PlayerPtr = Cast<II_Controller>(AiController);

				if (PlayerPtr) {  // ��������� ��� ���������� ����� ������� ��� ����, ����� �� ��������� ������������
					ConnectedControllers.Add(AiController);
					SpawnCamera(PlayerIndexMarker, true);
					//PlayerPtr->I_ROC_GetGameInstance(PlayerIndexMarker);
					PlayerIndexMarker++;
				}
			}
		}
	}

}


void AMaineGameMode::KefirDrop()
{
	
	for (APlayerCamerPawn* element : PalayersPawn)
	{
		if (element)
		{
			element->KefirAddInGame = true;
		}
	}

	KefirInGameSpawn = true;
	
}


