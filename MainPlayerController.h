// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
//#include "GameFramework/Controller.h"
#include "Alco/Interface/I_Controller.h"
#include "Net/UnrealNetwork.h"
#include "Alco/Core/MasterController.h"
#include "Alco/Player/PlayerCamerPawn.h"
//#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

/**
 * 
 */


UCLASS()
class ALCO_API AMainPlayerController : public AMasterController, public II_Controller
{
	GENERATED_BODY()
	

public:
	AMainPlayerController();
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		FName GetMyName();
		virtual FName GetMyName_Implementation();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SetMyName(FName Name);
		virtual void SetMyName_Implementation(FName Name);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		uint8 GetSelectedHeroe();
		virtual uint8 GetSelectedHeroe_Implementation();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SetSelectedHeroe(uint8 value);
		virtual void SetSelectedHeroe_Implementation(uint8 value);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		UTexture2D* GetPlayerIcon();
		virtual UTexture2D* GetPlayerIcon_Implementation();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SetPlayerIcon(UTexture2D* value);
		virtual void SetPlayerIcon_Implementation(UTexture2D* value);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		TSubclassOf<APlayerCamerPawn> GetSpawnActorClass();
		virtual TSubclassOf<APlayerCamerPawn> GetSpawnActorClass_Implementation();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SetSpawnActorClass(TSubclassOf<APlayerCamerPawn> value);
		virtual void SetSpawnActorClass_Implementation(TSubclassOf<APlayerCamerPawn> value);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		bool GetSpawnedVal();
		virtual bool GetSpawnedVal_Implementation();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void SetSpawnedVal(bool val);
		virtual void SetSpawnedVal_Implementation(bool val);

	virtual void I_ROC_GetGameInstance(uint8 playerid) override;
	virtual void I_ROS_GetGameInstance(FName PlayerNameValue, uint8 SelectedHeroeValue, 
					UTexture2D* PlayerIconValue, TSubclassOf<APlayerCamerPawn> SpawnActorClassValue,
					uint8 playerid) override;

	virtual void I_LevelStart() override;

	virtual AController* GetControllerRef() override;

private:
	UPROPERTY(Replicated)
		UGameInstance* GameInstanceRef = nullptr;
	UPROPERTY(Replicated)
		APlayerCamerPawn* CameraPtr;
	
	//���� ������ ������
	UPROPERTY(Replicated)
		FName MyName;
	UPROPERTY(Replicated)
		uint8 SelectedHeroe = -1;
	UPROPERTY(Replicated)
		UTexture2D* PlayerIcon = nullptr;
	UPROPERTY(Replicated)
		TSubclassOf<APlayerCamerPawn> SpawnActorClass;

		bool Spawned = true;

	UFUNCTION(Client, Reliable, BlueprintCallable)
		void ROC_GetGameInstance(uint8 playerid); // �������� ������� �������
	UFUNCTION(Server, Reliable)
		void ROS_GetGameInstance(FName PlayerNameValue, uint8 SelectedHeroeValue, UTexture2D* PlayerIconValue, TSubclassOf<APlayerCamerPawn> SpawnActorClassValue, uint8 playerid); // �������� ������� �������

	void LevelStart();

public:
	float MousePosX = 0.5f;
	float MousePosY = 0.5f;

	// ������� �������
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void MouseRollUp();
	UFUNCTION()
		void MouseRollDown();
	UFUNCTION()
		void LMC();
	UFUNCTION()
		void RMC();
	UFUNCTION()
		void MenueReqest();

	void MouseMovement(float DT);

};
